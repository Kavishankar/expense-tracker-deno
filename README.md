<h1> Expense Tracker </h1>
Made with Deno and :heart:

# Installation:
* Install latest version on Deno available from [here](https://deno.land/#installation)

# Frameworks:
* [Oak](https://deno.land/x/oak)

# Backend API:
Run the following command in terminal: `deno run -A mod.ts`

# Roadmap:
- [x] Initialize the project
- [ ] Backend CRUD API for Expenses
- [ ] Users support
- [ ] Add Frontend
